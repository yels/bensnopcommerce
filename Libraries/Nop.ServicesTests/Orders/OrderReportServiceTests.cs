﻿using Moq;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Services.Helpers;
using Nop.Services.Orders;
using Nop.Services.Tests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.ServicesTests.Orders
{
    class OrderReportServiceTests : ServiceTest
    {
        private readonly OrderReportService _orderReportService;
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            IMock<CatalogSettings> catalogSettings = new Mock<CatalogSettings>();
            IMock<IDateTimeHelper> dateTimeHelper = new Mock<DateTimeHelper>();
            IMock<IRepository<Order>> orderRepository = new Mock<IRepository<Order>>();
            IMock<IRepository<OrderItem>> orderItemRepository = new Mock<IRepository<OrderItem>>();



            _orderReportService = new OrderReportService();
        }
    }
}
