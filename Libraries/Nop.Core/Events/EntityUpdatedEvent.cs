﻿using System.Collections.Generic;

namespace Nop.Core.Events
{
    /// <summary>
    /// A container for entities that are updated.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityUpdatedEvent<T> where T : BaseEntity
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="entity">Entity</param>
        public EntityUpdatedEvent(T entity)
        {
            Entity = entity;
            Dictionary = new Dictionary<string, string>();
        }

        /// <summary>
        /// Entity
        /// </summary>
        public T Entity { get; }

        public Dictionary<string, string> Dictionary { get; set; }
    }
}
