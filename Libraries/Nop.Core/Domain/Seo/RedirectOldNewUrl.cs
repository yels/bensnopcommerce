﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Seo
{
    public class RedirectOldNewUrl: BaseEntity
    {
        public string OldUrl { get; set; }
        public string NewUrl { get; set; }
    }
}
