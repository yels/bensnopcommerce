﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.Seo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Seo
{
    public class RedirectOldNewUrlMap: NopEntityTypeConfiguration<RedirectOldNewUrl>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<RedirectOldNewUrl> builder)
        {
            builder.ToTable(nameof(RedirectOldNewUrl));
            builder.HasKey(record => record.Id);

            builder.Property(record => record.NewUrl).HasMaxLength(400).IsRequired();
            builder.Property(record => record.OldUrl).HasMaxLength(400).IsRequired();

            base.Configure(builder);
        }

        #endregion
    }
}
