﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Components
{
    public class ArmrestNavigationViewComponent : NopViewComponent
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private const int _specTypeId = 20;

        public ArmrestNavigationViewComponent(CatalogSettings catalogSettings, ICatalogModelFactory catalogModelFactory)
        {
            this._catalogSettings = catalogSettings;
            this._catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(int currentCatelogId)
        {
            if (_catalogSettings.ManufacturersBlockItemsToDisplay == 0)
                return Content("");

            var urlSpecs = _catalogModelFactory.GetActiveCategorySlugUrlRecords(this.Request.Path.Value).ToList();
            var specsString = string.Join(",", urlSpecs);
            var model = _catalogModelFactory.PrepareSpecAttributeOptionFilter(currentCatelogId, _specTypeId, specsString);

            var categorySpecsModel = _catalogModelFactory.PrepareSpecAttributeOptionFilter(currentCatelogId, _specTypeId, string.Empty);
            model.AlreadyFilteredItems = categorySpecsModel.Specs.Where(x => urlSpecs.Contains(x.Slug)).ToList();
            model.NotFilteredItems = new List<SpecificationAttributeOption>();
            model.Specs.ForEach(model.NotFilteredItems.Add);
            // this is the category of the url
            model.BasePath = this.Request.Path.Value;


            if (urlSpecs.Count() > 0)
            {
                foreach (var specSlug in urlSpecs)
                {
                    var material = model.Specs.FirstOrDefault(x => x.Slug == specSlug);
                    if (material != null)
                    {
                        model.NotFilteredItems.Remove(material);
                    }
                }
            }

            return View(model);
        }
    }
}
