﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Components
{
    public class PriceFilterNavigationViewComponent : NopViewComponent
    {
        //private readonly CatalogSettings _catalogSettings;
        //private readonly ICatalogModelFactory _catalogModelFactory;
        //private readonly CatalogPagingFilteringModel.PriceRangeFilterModel _priceRangeFilterModel;

        public PriceFilterNavigationViewComponent()
        {

        }

        //public PriceFilterNavigationViewComponent(CatalogPagingFilteringModel.PriceRangeFilterModel priceRangeFilterModel)
        //{
        //    //this._catalogSettings = catalogSettings;
        //    //this._catalogModelFactory = catalogModelFactory;
        //    this._priceRangeFilterModel = priceRangeFilterModel;
        //}

        public IViewComponentResult Invoke()
        {
            //if (_priceRangeFilterModel.Items.Count == 0)
            //    return Content("");

            return View();
        }
    }
}
