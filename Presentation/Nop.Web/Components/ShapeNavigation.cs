﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;

namespace Nop.Web.Components
{
    public class ShapeNavigationViewComponent : NopViewComponent
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private int _specTypeId = 41;

        public ShapeNavigationViewComponent(CatalogSettings catalogSettings, ICatalogModelFactory catalogModelFactory)
        {
            this._catalogSettings = catalogSettings;
            this._catalogModelFactory = catalogModelFactory;
        }

        public IViewComponentResult Invoke(int currentCatelogId)
        {
            if (_catalogSettings.ManufacturersBlockItemsToDisplay == 0)
                return Content("");

            var urlSpecs = _catalogModelFactory.GetActiveCategorySlugUrlRecords(this.Request.Path.Value).ToList();
            var specsString = string.Join(",", urlSpecs);
            var model = _catalogModelFactory.PrepareSpecAttributeOptionFilter(currentCatelogId, _specTypeId, specsString);

            var categorySpecsModel = _catalogModelFactory.PrepareSpecAttributeOptionFilter(currentCatelogId, _specTypeId, string.Empty);
            model.AlreadyFilteredItems = categorySpecsModel.Specs.Where(x => urlSpecs.Contains(x.Slug)).ToList();

            model.NotFilteredItems = new List<SpecificationAttributeOption>();
                //= categorySpecsModel.Specs.ToList();
            foreach (var item in categorySpecsModel.Specs)
            {                
                if (!model.NotFilteredItems.Any(x => x.Name == item.Name) && !urlSpecs.Contains(item.Slug))
                {
                    model.NotFilteredItems.Add(item);
                }
                else
                {
                    model.NotFilteredItems.Remove(model.NotFilteredItems.FirstOrDefault(x => x.Id == item.Id));
                }
            }

            // this is the category of the url
            model.BasePath = this.Request.Path.Value;

            if (urlSpecs.Count() > 0)
            {
                foreach (var specSlug in urlSpecs)
                {
                    var shape = model.Specs.FirstOrDefault(x => x.Slug == specSlug);
                    if (shape != null)
                    {
                        model.NotFilteredItems.Remove(shape);
                    }
                }
            }

            return View(model);
        }
    }
}
