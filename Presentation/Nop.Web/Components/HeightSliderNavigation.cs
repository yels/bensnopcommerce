﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Common;

namespace Nop.Web.Components
{
    public class HeightSliderNavigationViewComponent : NopViewComponent
    {
        private readonly CatalogSettings _catalogSettings;
        private readonly ICatalogModelFactory _catalogModelFactory;
        private readonly IRepository<SpecificationAttributeOption> _specificationAttributeRepository;
        private int _specTypeId = 7;

        public HeightSliderNavigationViewComponent(CatalogSettings catalogSettings, ICatalogModelFactory catalogModelFactory, IRepository<SpecificationAttributeOption> specificationAttributeRepository)
        {
            this._catalogSettings = catalogSettings;
            this._catalogModelFactory = catalogModelFactory;
            this._specificationAttributeRepository = specificationAttributeRepository;
        }

        public IViewComponentResult Invoke(int currentCatelogId)
        {
            if (_catalogSettings.ManufacturersBlockItemsToDisplay == 0)
                return Content("");

            var attributesList = _specificationAttributeRepository.Table
                .Where(x => x.SpecificationAttributeId == 7 && x.Slug != null && x.Slug.StartsWith('h'))
                .ToList();

            var attributes = attributesList
                .Select(x => TryParseName(x.Slug))
                .ToList();

            var model = new SliderRangeModel
            {
                MinValue = 1,
                MaxValue = 250,
            };

            return View(model);
        }

        private int TryParseName(string x)
        {
            try
            {
                return int.Parse(x.Replace("h-", "").Trim());
            }
            catch (Exception ex)
            {
                return 1;
            }
        }
    }
}
