﻿using Nop.Core.Domain.Catalog;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Factories
{
    public partial class SpecificationModelFactory
    {
        /// <summary>
        /// Specification filter model
        /// </summary>
        public SpecificationModelFactory()
        {
            this.Colors = new List<SpecificationAttributeOption>();
            this.NotFilteredItems = new List<SpecificationAttributeOption>();
            this.AlreadyFilteredItems = new List<SpecificationAttributeOption>();
            this.DictQueryString = new Dictionary<string, string>();
        }

        /// <summary>
        /// Not filtered yet items
        /// </summary>
        public IList<SpecificationAttributeOption> NotFilteredItems { get; set; }
        public IList<SpecificationAttributeOption> AlreadyFilteredItems { get; set; }
        public List<SpecificationAttributeOption> Colors { get; set; }
        public CatalogPagingFilteringModel RemoveFilterUrl { get; set; }

        public string BasePath { get; set; }

        public Dictionary<string, string> DictQueryString { get; set; }
        public string GetQueryString(string spec)
        {
            List<string> keys = new List<string>();

            if (DictQueryString.Count() > 0)
            {
                var dict = DictQueryString.GetValueOrDefault("specs");
                if (dict == null)
                {
                    DictQueryString.Add("specs", spec);
                }

                foreach (KeyValuePair<string, string> keyValue in DictQueryString.ToList())
                {
                    var queryString = $"{keyValue.Key}={keyValue.Value}";
                    if (keyValue.Key.Contains("specs"))
                    {
                        queryString += $",{spec}";
                    }

                    keys.Add(queryString);
                }
                return $"?{ string.Join('&', keys)}";
            }

            return $"?specs={spec}";
        }

        public string RemoveSpecFromQueryString()
        {
            DictQueryString.Remove("specs");

            if (DictQueryString.Count() > 0)
            {
                var keys = new List<string>();
                DictQueryString.ToList().ForEach(x => keys.Add($"{x.Key}={x.Value}"));

                return $"?{ string.Join('&', keys)}";
            }

            return BasePath;
        }
    }
}
