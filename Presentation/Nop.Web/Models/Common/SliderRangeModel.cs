﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Common
{
    public class SliderRangeModel
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}
