﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Web.Framework.Models;
using Nop.Web.Infrastructure.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Nop.Web.Models.Catalog.CatalogPagingFilteringModel;

namespace Nop.Web.Models.Catalog
{
    public partial class SpecificationFilterModel : BaseNopEntityModel
    {
        /// <summary>
        /// Specification filter model
        /// </summary>
        public SpecificationFilterModel()
        {
            this.Specs = new List<SpecificationAttributeOption>();
            this.NotFilteredItems = new List<SpecificationAttributeOption>();
            this.AlreadyFilteredItems = new List<SpecificationAttributeOption>();
        }

        /// <summary>
        /// Not filtered yet items
        /// </summary>
        public IList<SpecificationAttributeOption> NotFilteredItems { get; set; }
        /// <summary>
        /// already filtered yet items
        /// </summary>
        public IList<SpecificationAttributeOption> AlreadyFilteredItems { get; set; }
        public List<SpecificationAttributeOption> Specs { get; set; }
        public CatalogPagingFilteringModel RemoveFilterUrl { get; set; }    
        
        public string BasePath { get; set; }
        
        public string GetQueryString(string spec)
        {
            var result = BasePath.Substring(BasePath.Length - 1);
            if (result == "/")
            {
                var newPath = BasePath.Remove(BasePath.Length - 1);
                return $"{newPath}/{spec}/";
            }
            return $"{BasePath}/{spec}/";
        }

        public string RemoveSpecFromQueryString()
        {
            foreach (var filter in AlreadyFilteredItems)
            {
                BasePath = BasePath.Replace($@"/{filter.Slug}", "");
            }

            return BasePath;
        }
    }
}