﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Web.Framework.Models;
using Nop.Web.Infrastructure.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Nop.Web.Models.Catalog.CatalogPagingFilteringModel;

namespace Nop.Web.Models.Catalog
{
    public partial class ColorSpecificationFiltersModel : BaseNopModel
    {
        /// <summary>
        /// Specification filter model
        /// </summary>
        public ColorSpecificationFiltersModel()
        {
            this.Colors = new List<SpecificationAttributeOption>();
            this.NotFilteredItems = new List<SpecificationAttributeOption>();
            this.AlreadyFilteredItems = new List<SpecificationAttributeOption>();
        }

        /// <summary>
        /// Not filtered yet items
        /// </summary>
        public IList<SpecificationAttributeOption> NotFilteredItems { get; set; }
        public IList<SpecificationAttributeOption> AlreadyFilteredItems { get; set; }
        public List<SpecificationAttributeOption> Colors { get; set; }
        public CatalogPagingFilteringModel RemoveFilterUrl { get; set; }    
        
        public string BasePath { get; set; }

        public string GetQueryString(string spec)
        {
            return $"{BasePath}/{spec}";
        }

        public string RemoveSpecFromQueryString()
        {
            foreach (var filter in AlreadyFilteredItems)
            {
                BasePath = BasePath.Replace($@"/{filter.Slug}", "");
            }

            return BasePath;
        }
    }
}