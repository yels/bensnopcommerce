﻿using Nop.Services.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Catalog
{
    public class PriceRangeFilterModel
    {
        #region properties
        public decimal FromProductPrice { get; set; }
        public decimal ToProductPrice { get; set; }
        public Dictionary<string, decimal> PriceRangeFilterValuesDictionary { get; set; }
        #endregion
        public PriceRangeFilterModel(decimal fromMinimalProductPrice, decimal toMaximalProductPrice)
        {
            FromProductPrice = fromMinimalProductPrice;
            ToProductPrice = toMaximalProductPrice;
            //PriceRangeValues t = new PriceRangeValues();
            PriceRangeValues priceRangeValues = new PriceRangeValues(FromProductPrice, ToProductPrice);
            PriceRangeFilterValuesDictionary = priceRangeValues.GetPriceRangeFilterValues();
        }
    }
}
