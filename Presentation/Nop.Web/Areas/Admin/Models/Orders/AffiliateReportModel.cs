﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Orders
{
    public class AffiliateReportModel : BaseNopModel
    {
        public string AffiliateName { get; set; }
        public int AmountOfOrders { get; set; }
        public int AmountOfUniqueCustomers { get; set; }
        public string TotalAmountOrdered { get; set; }
        public string AverageAmountPerCustomer { get; internal set; }
    }
}
