﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Localization;
using Nop.Core.Infrastructure;
using Nop.Services.Events;
using Nop.Services.Seo;
using Nop.Web.Framework.Localization;
using static Nop.Services.Seo.UrlRecordService;

namespace Nop.Web.Framework.Seo
{
    /// <summary>
    /// Provides properties and methods for defining a SEO friendly route, and for getting information about the route.
    /// </summary>
    public class GenericPathRoute : LocalizedRoute
    {
        #region Fields
        private readonly IRouter _target;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IStaticCacheManager _cacheManager;        
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="target">Target</param>
        /// <param name="routeName">Route name</param>
        /// <param name="routeTemplate">Route template</param>
        /// <param name="defaults">Defaults</param>
        /// <param name="constraints">Constraints</param>
        /// <param name="dataTokens">Data tokens</param>
        /// <param name="inlineConstraintResolver">Inline constraint resolver</param>
        public GenericPathRoute(IRouter target, string routeName, string routeTemplate, RouteValueDictionary defaults,
            IDictionary<string, object> constraints, RouteValueDictionary dataTokens, IInlineConstraintResolver inlineConstraintResolver, IUrlRecordService urlRecordService)
            : base(target, routeName, routeTemplate, defaults, constraints, dataTokens, inlineConstraintResolver)
        {
            _target = target ?? throw new ArgumentNullException(nameof(target));
            _urlRecordService = urlRecordService;

        }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="target">Target</param>
        /// <param name="routeName">Route name</param>
        /// <param name="routeTemplate">Route template</param>
        /// <param name="defaults">Defaults</param>
        /// <param name="constraints">Constraints</param>
        /// <param name="dataTokens">Data tokens</param>
        /// <param name="inlineConstraintResolver">Inline constraint resolver</param>
        public GenericPathRoute(IRouter target, string routeName, string routeTemplate, RouteValueDictionary defaults,
            IDictionary<string, object> constraints, RouteValueDictionary dataTokens, IInlineConstraintResolver inlineConstraintResolver)
            : base(target, routeName, routeTemplate, defaults, constraints, dataTokens, inlineConstraintResolver)
        {
            _target = target ?? throw new ArgumentNullException(nameof(target));
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Get route values for current route
        /// </summary>
        /// <param name="context">Route context</param>
        /// <returns>Route values</returns>
        protected RouteValueDictionary GetRouteValues(RouteContext context)
        {
            //remove language code from the path if it's localized URL
            var path = context.HttpContext.Request.Path.Value;

            var categoryPath = path;
            if (this.SeoFriendlyUrlsForLanguagesEnabled && path.IsLocalizedUrl(context.HttpContext.Request.PathBase, false, out Language _))
                path = path.RemoveLanguageSeoCodeFromUrl(context.HttpContext.Request.PathBase, false);

            //parse route data
            var routeValues = new RouteValueDictionary(this.ParsedTemplate.Parameters
                .Where(parameter => parameter.DefaultValue != null)
                .ToDictionary(parameter => parameter.Name, parameter => parameter.DefaultValue));
            var matcher = new TemplateMatcher(this.ParsedTemplate, routeValues);

            var splittedPath = path.Split(@"/").ToList();
            if (string.IsNullOrEmpty(splittedPath.LastOrDefault()))
            {
                var length = splittedPath.Count();
                splittedPath.RemoveAt(length - 1);
            }
            if (splittedPath.Count > 0 && _urlRecordService != null)
            {
                //ColorNavigation, SeatNavifation, ArmrestNavigation, VendorNavigation, MaterialNavigation, StyleNavigation, ManufacturerNavigation
                var categoriesDb = _urlRecordService.GetAllActiveCategoryUrlRecords()
                .Select(x => x.Slug).ToList();
                var specsDb = _urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(20).ToList();
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(6));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(5));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(57));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(9));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(85));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(1));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(7));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(28));
                specsDb.AddRange(_urlRecordService.GetSpecificationAttributeOptionsBySpecificationAttribute(41));

                var categories = new List<string>();
                var specs = new List<string>();
                for (int i = 0; i <= splittedPath.Count() - 1; i++)
                {
                    if (i > 0)
                    {
                        var subPath = splittedPath[i];
                        if (subPath.StartsWith("sl-") && subPath.Length > 4)
                        {
                            specs.Add(subPath);
                        }
                        if (categoriesDb.Contains(subPath))
                        {
                            categories.Add(subPath);
                        }
                        else if (specsDb.Contains(subPath))
                        {
                            specs.Add(subPath);
                        }
                    }
                }

                var tempPath = categoryPath.Split(@"/").ToList();
                if (string.IsNullOrWhiteSpace(tempPath.Last()) && tempPath.Count > 0)
                {
                    tempPath.RemoveAt(tempPath.Count - 1);
                }
                if (string.IsNullOrWhiteSpace(tempPath.First()) && tempPath.Count > 0)
                {
                    tempPath.RemoveAt(0);
                }

                if (path.Contains("/product/"))
                {
                    var b = matcher.TryMatch(@"/" + tempPath.Last(), routeValues);
                    routeValues.Add("ProductOrManufacturer", tempPath.Last());
                }
                else if (path.Contains("/merken/"))
                {
                    var b = matcher.TryMatch(@"/" + tempPath.Last(), routeValues);
                    routeValues.Add("ProductOrManufacturer", tempPath.Last());
                }
                else if (categories.Count > 0 && (categories.Count + specs.Count) == tempPath.Count)
                {
                    path = @"/" + categories[categories.Count - 1];
                    var b = matcher.TryMatch(@"/" + categories.Last(), routeValues);
                    routeValues.Add("OnlyCategory", categories.Last());
                }
            }

            matcher.TryMatch(path, routeValues);

            return routeValues;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Route request to the particular action
        /// </summary>
        /// <param name="context">A route context object</param>
        /// <returns>Task of the routing</returns>
        public override Task RouteAsync(RouteContext context)
        {
            if (!DataSettingsManager.DatabaseIsInstalled)
                return Task.CompletedTask;
                        
            var path = context.HttpContext.Request.Path;


            if (_urlRecordService != null)
            {           
                var allRedirectOldNew = _urlRecordService.GetRedirectAllOldNewUrls();
                var oldUrlMatch = allRedirectOldNew.FirstOrDefault(x => x.OldUrl == path);
                if (oldUrlMatch != null)
                {
                    //redirect to active slug if found
                    var redirectionRouteData = new RouteData(context.RouteData);
                    redirectionRouteData.Values["controller"] = "Common";
                    redirectionRouteData.Values["action"] = "InternalRedirect";
                    redirectionRouteData.Values["url"] = $"{oldUrlMatch.NewUrl}";
                    redirectionRouteData.Values["permanentRedirect"] = true;
                    context.HttpContext.Items["nop.RedirectFromGenericPathRoute"] = true;
                    context.RouteData = redirectionRouteData;

                    _target.RouteAsync(context);
                    return Task.CompletedTask;
                }
            }

            //try to get slug from the route data
            var routeValues = GetRouteValues(context);

            //performance optimization, we load a cached verion here. It reduces number of SQL requests for each page load
            var urlRecordService = EngineContext.Current.Resolve<IUrlRecordService>();

            //comment the line above and uncomment the line below in order to disable this performance "workaround"
            //var urlRecord = urlRecordService.GetBySlug(slug);

            var checkUrlItemsExists = CheckIfUrlItemsExists(context);
            if (!checkUrlItemsExists)
                return Task.CompletedTask;

            if (!routeValues.TryGetValue("GenericSeName", out object slugValue) || string.IsNullOrEmpty(slugValue as string))
            {
                if (routeValues.ContainsKey("GenericSeName"))
                {
                    return Task.CompletedTask;
                }
            }

            var slug = slugValue as string;
            var urlRecord = urlRecordService.GetBySlugCached(slug);

            if (urlRecord == null)
                return Task.CompletedTask;

            var targetRoute = ExtraUrlSlugChecks(context, urlRecord, urlRecordService, slug);

            //route request
            return targetRoute;
        }

        private bool CheckUrlAreFound(RouteValueDictionary routeValues, RouteContext context, UrlRecordForCaching urlRecord, string slugValue, string slugOnlyCategory, string slugCategory, string slugSecondCategory, string slugProductManufacturer)
        {
            var urlRecordService = EngineContext.Current.Resolve<IUrlRecordService>();
            var urlOnlyCategory = urlRecordService.GetBySlug(slugOnlyCategory);
            var urlCategory = urlRecordService.GetBySlug(slugCategory);
            var urlSecondCategory = urlRecordService.GetBySlug(slugSecondCategory);
            var urlProductManufacturer = urlRecordService.GetBySlug(slugProductManufacturer);

            //no URL record found
            if (urlRecord == null)
            {
                if (routeValues.ContainsKey("GenericSeName"))
                {
                    return true;
                }
            }

            //no URL record for category only found
            if (urlOnlyCategory == null)
            {
                if (routeValues.ContainsKey("OnlyCategory"))
                {
                    return true;
                }
            }
            //no URL record category found
            if (urlCategory == null)
            {
                if (routeValues.ContainsKey("Category"))
                {
                    return true;
                }
            }

            //no URL record for the second category found
            if (urlSecondCategory == null)
            {
                if (routeValues.ContainsKey("SecondCategory"))
                {
                    return true;
                }
            }

            //no URL record with a product or manufacturer found
            if (urlProductManufacturer == null)
            {
                if (routeValues.ContainsKey("ProductOrManufacturer"))
                {
                    return true;
                }
            }

            //route request
            return false;
        }

        private bool CheckIfUrlItemsExists(RouteContext context)
        {
            //performance optimization, we load a cached verion here. It reduces number of SQL requests for each page load
            var urlRecordService = EngineContext.Current.Resolve<IUrlRecordService>();

            //try to get slug from the route data
            var routeValues = GetRouteValues(context);

            if (!routeValues.TryGetValue("GenericSeName", out object slugValue) || string.IsNullOrEmpty(slugValue as string))
            {
                if (routeValues.ContainsKey("GenericSeName"))
                {
                    return true;
                }
            }

            if (!routeValues.TryGetValue("ProductOrManufacturer", out object slugProductManufacturerValue) || string.IsNullOrEmpty(slugProductManufacturerValue as string))
            {
                if (routeValues.ContainsKey("ProductOrManufacturer"))
                {
                    return true;
                }
            }

            // this check if there is only category
            if (!routeValues.TryGetValue("OnlyCategory", out object slugOnlyCategoryValue) || string.IsNullOrEmpty(slugOnlyCategoryValue as string))
            {
                if (routeValues.ContainsKey("OnlyCategory"))
                {
                    return true;
                }
            }

            // this check is for the first category
            if (!routeValues.TryGetValue("Category", out object slugCategoryValue) || string.IsNullOrEmpty(slugCategoryValue as string))
            {
                if (routeValues.ContainsKey("Category"))
                {
                    return true;
                }
            }

            // this check is for the second category
            if (!routeValues.TryGetValue("SecondCategory", out object slugSecondCategoryValue) || string.IsNullOrEmpty(slugSecondCategoryValue as string))
            {
                if (routeValues.ContainsKey("SecondCategory"))
                {
                    return true;
                }
            }

            var slug = slugValue as string;
            var slugOnlyCategory = slugOnlyCategoryValue as string;
            var slugCategory = slugCategoryValue as string;
            var slugSecondCategory = slugSecondCategoryValue as string;
            var slugProductManufacturer = slugProductManufacturerValue as string;

            var urlRecord = urlRecordService.GetBySlugCached(slug);

            var urlFound = CheckUrlAreFound(routeValues, context, urlRecord, slug, slugOnlyCategory, slugCategory, slugSecondCategory, slugProductManufacturer);
            //ExtraUrlSlugChecks(context, urlRecord, urlRecordService, slug);
            if (urlFound)
            {
                return false;
            }

            //route request
            return true;
        }

        private Task ExtraUrlSlugChecks(RouteContext context, UrlRecordForCaching urlRecord, IUrlRecordService urlRecordService, string slug)
        {
            //virtual directory path
            var pathBase = context.HttpContext.Request.PathBase;

            //if URL record is not active let's find the latest one
            if (!urlRecord.IsActive)
            {
                var activeSlug = urlRecordService.GetActiveSlug(urlRecord.EntityId, urlRecord.EntityName, urlRecord.LanguageId);
                if (string.IsNullOrEmpty(activeSlug))
                    return Task.CompletedTask;

                //redirect to active slug if found
                var redirectionRouteData = new RouteData(context.RouteData);
                redirectionRouteData.Values["controller"] = "Common";
                redirectionRouteData.Values["action"] = "InternalRedirect";
                redirectionRouteData.Values["url"] = $"{pathBase}/{activeSlug}{context.HttpContext.Request.QueryString}";
                redirectionRouteData.Values["permanentRedirect"] = true;
                context.HttpContext.Items["nop.RedirectFromGenericPathRoute"] = true;
                context.RouteData = redirectionRouteData;
                return _target.RouteAsync(context);
            }

            var category = "";
            var path = context.HttpContext.Request.Path;
            //added a check that it only looks at the last part of the url
            var temp = path.Value.Split(@"/");
            if (temp.Length > 0)
            {
                category = temp[1];
            }

            //ensure that the slug is the same for the current language, 
            //otherwise it can cause some issues when customers choose a new language but a slug stays the same
            var slugForCurrentLanguage = urlRecordService.GetSeName(urlRecord.EntityId, urlRecord.EntityName);
            if (!string.IsNullOrEmpty(slugForCurrentLanguage) && !slugForCurrentLanguage.Equals(slug, StringComparison.InvariantCultureIgnoreCase))
            {
                //we should make validation above because some entities does not have SeName for standard (Id = 0) language (e.g. news, blog posts)

                //redirect to the page for current language
                var redirectionRouteData = new RouteData(context.RouteData);
                redirectionRouteData.Values["controller"] = "Common";
                redirectionRouteData.Values["action"] = "InternalRedirect";
                redirectionRouteData.Values["url"] = $"{pathBase}/{slugForCurrentLanguage}{context.HttpContext.Request.QueryString}";
                redirectionRouteData.Values["permanentRedirect"] = false;
                context.HttpContext.Items["nop.RedirectFromGenericPathRoute"] = true;
                context.RouteData = redirectionRouteData;
                return _target.RouteAsync(context);
            }

            //since we are here, all is ok with the slug, so process URL
            var currentRouteData = new RouteData(context.RouteData);
            switch (urlRecord.EntityName.ToLowerInvariant())
            {
                case "product":
                    currentRouteData.Values["controller"] = "Product";
                    currentRouteData.Values["CategoryName"] = category;
                    currentRouteData.Values["action"] = "ProductDetails";
                    currentRouteData.Values["productid"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "producttag":
                    currentRouteData.Values["controller"] = "Catalog";
                    currentRouteData.Values["action"] = "ProductsByTag";
                    currentRouteData.Values["productTagId"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "category":
                    currentRouteData.Values["controller"] = "Catalog";
                    currentRouteData.Values["action"] = "Category";
                    currentRouteData.Values["categoryid"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "manufacturer":
                    currentRouteData.Values["controller"] = "Catalog";
                    currentRouteData.Values["action"] = "Manufacturer";
                    currentRouteData.Values["manufacturerid"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "vendor":
                    currentRouteData.Values["controller"] = "Catalog";
                    currentRouteData.Values["action"] = "Vendor";
                    currentRouteData.Values["vendorid"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "newsitem":
                    currentRouteData.Values["controller"] = "News";
                    currentRouteData.Values["action"] = "NewsItem";
                    currentRouteData.Values["newsItemId"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "blogpost":
                    currentRouteData.Values["controller"] = "Blog";
                    currentRouteData.Values["action"] = "BlogPost";
                    currentRouteData.Values["blogPostId"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                case "topic":
                    currentRouteData.Values["controller"] = "Topic";
                    currentRouteData.Values["action"] = "TopicDetails";
                    currentRouteData.Values["topicId"] = urlRecord.EntityId;
                    currentRouteData.Values["SeName"] = urlRecord.Slug;
                    break;
                default:
                    //no record found, thus generate an event this way developers could insert their own types
                    EngineContext.Current.Resolve<IEventPublisher>()
                        ?.Publish(new CustomUrlRecordEntityNameRequestedEvent(currentRouteData, urlRecord));
                    break;
            }
            context.RouteData = currentRouteData;

            //route request
            return _target.RouteAsync(context);
        }
        #endregion
    }
}